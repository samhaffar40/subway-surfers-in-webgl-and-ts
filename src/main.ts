import { mat4 } from "./gl-matrix";
import { World } from "./world";
import { initWorld/* , GL_BL_ASSETS */ } from "./init";

import { /* initBuffers, */ mvPushMatrix, mvPopMatrix, /* loadShader, */ refreshPrograminfo, initShaderProgram } from "./utils";
// import * as THREE from "three";

var mainworld: World;

declare global {
	interface Program {
		program: WebGLProgram,
		attribLocations: {
			vertexPosition: number,
			vertexNormal: number,
			textureCoord: number
		},
		uniformLocations: {
			projectionMatrix: WebGLUniformLocation,
			modelViewMatrix: WebGLUniformLocation,
		},
	}
}

var LightPos: Vector3 = {
	x: 0,
	y: 2,
	z: 0
};

enum CurrentlyUsingis {
	Colored,
	Grayscale
};

window['LightPos'] = LightPos;


var flicker: number = 0;
var starttime: Date = new Date();
var currtime: Date = new Date();

var Zoom = 1;
window['Zoom'] = Zoom;

document.onwheel = (ev) => {
	// console.log(ev);
	// console.log(Zoom);
	if (ev.deltaY > 0) {
		/* Zoom out */
		Zoom++;
		mainworld.player.rotation.y += 10;
	} else {
		/* Zoom in */
		Zoom--;
		mainworld.player.rotation.y -= 10;
	}
	if (Zoom < 0) Zoom = 1;
	window['Zoom'] = Zoom;
	// console.log(mainworld.player.rotation.y);
}

declare global {
	interface Buffer {
		position: WebGLBuffer,
		normals: WebGLBuffer;
		indices: WebGLBuffer,
		texture: WebGLBuffer,
		vertcnt: number,
	}
	interface Element {
		hidden: boolean;
	}
};

const canvas: HTMLCanvasElement = document.querySelector('#game');
const gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');

const textCanvas: HTMLCanvasElement = document.querySelector('#text');
var ctx = textCanvas.getContext("2d");
ctx.font = "30px Arial";

var refresh : HTMLImageElement = document.querySelector('#refresh');

window['scorecanv'] = { textCanvas, ctx };

const vsSource = `
uniform highp mat4 uMVMatrix;
uniform highp mat4 uPMatrix;

uniform highp mat3 uNormalMatrix;
attribute highp vec3 aVertexPosition;
attribute highp vec2 aTextureCoord;
attribute highp vec3 aVertexNormal;
varying highp vec2 TextCoordInterp;
varying highp vec3 PositionInterp;

varying highp vec3 NormalInterp;

void main()
{
	PositionInterp = (uMVMatrix * vec4(aVertexPosition, 1.0)).xyz;
	TextCoordInterp = aTextureCoord;
	
	NormalInterp = normalize(uNormalMatrix * aVertexNormal);
	gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
}
`;

const fsSource = `
uniform highp sampler2D uSampler;

uniform highp vec3 LightPosition;

uniform highp vec3 LightColor;
varying highp vec2 TextCoordInterp;
varying highp vec3 NormalInterp;
varying highp vec3 PositionInterp;

void main()
{
	highp vec3 normal = normalize(NormalInterp);
	
	highp vec3 lightVec = normalize(LightPosition - PositionInterp);
	highp vec3 viewVec = normalize(-PositionInterp);

	highp vec3 reflectVec = reflect(-lightVec, normal);
	highp float spec = max(dot(reflectVec, viewVec), 0.0);
	spec = pow(spec, 16.0);
	
	highp vec4 textureColor = texture2D(uSampler, vec2(TextCoordInterp.s, TextCoordInterp.t));
	
	highp vec3 specContrib = LightColor * spec;
	
	highp vec3 ambientContrib = vec3(0.0, 0.0, 0.0);
	highp vec3 diffContrib = LightColor * max(dot(lightVec, normal), 0.0);
	
	highp vec3 lightContribution = ambientContrib + diffContrib + (specContrib * (1.0 - textureColor.a));
	
	`;
	
	var coloredFs = `
	// gl_FragColor = vec4(textureColor.rgb * lightContribution, 1.0);
	gl_FragColor = vec4(textureColor.rgb, 1.0);
}
`;

var greyscFs = `
	highp float gray = dot(textureColor.rgb, vec3(0.299, 0.587, 0.114));
	gl_FragColor = vec4(vec3(gray), 1.0);
}
`;

var currently_using: CurrentlyUsingis;
currently_using = CurrentlyUsingis.Colored;

var programInfo: Program;
var shaderProgram: WebGLProgram;

var main = () => {
	const loading: Element = document.querySelector('#loading');
	canvas.hidden = true;
	console.log(gl, 'is not undefined');

	// gl.viewport()

	// If we don't have a GL context, give up now

	if (!gl) {
		alert('Unable to initialize WebGL. Your browser or machine may not support it.');
		return;
	}

	shaderProgram = initShaderProgram(gl, vsSource, fsSource + coloredFs);

	programInfo = {
		program: shaderProgram,
		attribLocations: {
			vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
			textureCoord: gl.getAttribLocation(shaderProgram, "aTextureCoord"),
			vertexNormal: gl.getAttribLocation(shaderProgram, 'aVertexNormal'),
			/* vertexColor: gl.getAttribLocation(shaderProgram, 'aVertexColor'), */
		},
		uniformLocations: {
			projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
			modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
		},
	};

	// Here's where we call the routine that builds all the
	// objects we'll be drawing.
	initWorld(gl, programInfo, (world: World) => {
		mainworld = world;
		console.log("WORLD BUFFERS", world.player.buffers);
		console.log('Init world 2', mainworld);
		window['world'] = mainworld;
		const buffers = mainworld.player.buffers;
		// const buffers = mainworld.player.buffers;
		console.log("BUFFERS IN INIT CALLBACK", buffers);
		var then = 0;

		canvas.hidden = false;
		loading.hidden = true;

		// Draw the scene repeatedly
		function render(now: number) {
			now *= 0.001;  /* convert to seconds */
			const deltaTime = now - then;
			then = now;

			drawScene(gl, programInfo, buffers, deltaTime);

			requestAnimationFrame(render);
		};
		requestAnimationFrame(render);
	});
};

var rotation: Vector3;

var pooper: any[] = [];
var modelViewMatrix: Float32Array;
var cameraPosition: number[];

cameraPosition = [-2, 2, 0];
window['cameraPosition'] = cameraPosition;
var direction = [1, 2, 3];

var LightCOLO: Vector3 = {
	x: 1,
	y: 1,
	z: 1
};

window['LightCOLO'] = LightCOLO;

var update_zoom_eye = () => {
	if (canvas.onmousedown)
		cameraPosition[0] = mainworld.player.position.x + direction[0] * Zoom;
	cameraPosition[1] = mainworld.player.position.x + direction[0] * Zoom;
	cameraPosition[2] = mainworld.player.position.x + direction[0] * Zoom;
};

function drawScene(gl: WebGLRenderingContext, programInfo: Program, buffers: Buffer, deltaTime: number) {
	gl.clearColor(0.0, 1.0, 1.0, 1.0);  // Clear to black, fully opaque
	gl.clearDepth(1.0);                 // Clear everything
	gl.enable(gl.DEPTH_TEST);           // Enable depth testing
	gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	if (!mainworld.player.dead) {
		ctx.fillText(`Score`, 10, 30);
		ctx.fillText(`${mainworld.player.score}`, 200, 30);
		ctx.fillText(`Scates left`, 10, 60);
		ctx.fillText(`${mainworld.player.scateboardcount}`, 200, 60);

		ctx.fillText(`Chances`, 10, 90);
		ctx.fillText(`${2 - mainworld.player.confusedcount}`, 200, 90);
	}

	if (mainworld.player.dead) {
		ctx.fillStyle = "orangered";
		ctx.fillText(`GAME OVER`, 500 - 100, 350 - 15);
		ctx.fillText(`Score`, 500 - 100, 350 + 15);
		ctx.fillText(`${mainworld.player.score}`, 500 + 10, 350 + 15);
		ctx.fillText(`Coins`, 500 - 100, 350 + 45);
		ctx.fillText(`${mainworld.player.coincount}`, 500 + 10, 350 + 45);
		ctx.drawImage(refresh, 1, 1);
	}

	if (!mainworld.player.dead && mainworld.paused) {
		ctx.fillText(`Paused Press P to continue`, 500 - 200, 350 + 45);
	}
	// ctx.fillText(`Score \t${mainworld.player.score}`, 10, 100);

	currtime = new Date();
	if (currtime.getTime() - starttime.getTime() > 3000) {
		clearInterval(flicker);
	}

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	rotation = mainworld.player.rotation;

	gl.useProgram(programInfo.program);

	var LightPosition = gl.getUniformLocation(programInfo.program, "LightPosition");
	gl.uniform3f(LightPosition, LightPos.x, LightPos.y, LightPos.z);

	var LightColor = gl.getUniformLocation(programInfo.program, "LightColor");
	gl.uniform3f(LightColor, LightCOLO.x, LightCOLO.y, LightCOLO.z);

	const fieldOfView = 45 * Math.PI / 180;   // in radians
	const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
	const zNear = 0.1;
	const zFar = 1000.0;
	const projectionMatrix = mat4.create();

	mat4.perspective(projectionMatrix,
		fieldOfView,
		aspect,
		zNear,
		zFar);

	var cameraMatrix = mat4.create();
	mat4.translate(
		cameraMatrix,
		cameraMatrix,
		[
			mainworld.cameraPos.x,
			mainworld.cameraPos.y,
			mainworld.cameraPos.z
		]);

	var up = [0, 1, 0];
	var player_coords = [
		mainworld.player.position.x,
		mainworld.player.position.y,
		mainworld.player.position.z
	];

	mat4.lookAt(
		cameraMatrix,
		[
			mainworld.cameraPos.x,
			mainworld.cameraPos.y,
			mainworld.cameraPos.z,
		],
		[
			mainworld.cameraPos.x + 90,
			0,
			/* mainworld.cameraPos.z */ + 0,
		] /* player_coords */, up);

	var viewMatrix = cameraMatrix;

	var viewProjectionMatrix = mat4.create();

	mat4.multiply(viewProjectionMatrix, projectionMatrix, viewMatrix);

	modelViewMatrix = mat4.create();
	window['mat4'] = mat4;

	mat4.translate(
		modelViewMatrix,     /* destination matrix */
		modelViewMatrix,     /* matrix to translate */
		[-0.0, 0.0, 0],      /* amount to translate */
	);

	mvPushMatrix(pooper, modelViewMatrix);

	mainworld.draw(viewProjectionMatrix, programInfo, deltaTime);

	mvPopMatrix(pooper);

	// Update the rotation for the next draw
	!mainworld.paused && mainworld.tickall();
	// renderer.render(scene, camera);
}


document.onkeyup = (ev: KeyboardEvent): void => {
	// console.log(ev);
	if (ev.key == "a" || ev.key == "ArrowLeft") {
		if (mainworld.player.switchinglane) {
			console.log('already switching left');
			return;
		}
		if (mainworld.player.lane != -1) {
			mainworld.player.switchinglane = true;
			mainworld.player.movingleft = true;
			mainworld.player.switchstartpos = mainworld.player.position.z;
			mainworld.player.nextlane = -1;
			console.log('left');
		}
		if (mainworld.player.lane == -1){
			mainworld.player.isconfused = true;
		}
	} else if (ev.key == "d" || ev.key == "ArrowRight") {
		if (mainworld.player.switchinglane) {
			console.log('already switching right');
			return;
		}
		if (mainworld.player.lane != 1) {
			mainworld.player.switchinglane = true;
			mainworld.player.movingright = true;
			mainworld.player.switchstartpos = mainworld.player.position.z;
			mainworld.player.nextlane = 1;
			console.log('right');
		}
		if (mainworld.player.lane == 1){
			mainworld.player.isconfused = true;
		}
	}

	if (ev.key == "h") {
		mainworld.player.activateScateboard();
	}

	if (ev.key == "j") {
		mainworld.player.activateShoes();
	}

	if (ev.key == " " || ev.key == "ArrowUp") {

		if (mainworld.player.jumping) {
			console.log("already jumping");
			return;
		}
		else {
			mainworld.player.didstunt = Math.random() > 0.3;
			mainworld.player.jumping = true;
		}
	}
	if (ev.key == "s" || ev.key == "ArrowDown") {

		if (mainworld.player.crouching) {
			console.log("already crouching");
			return;
		}
		else {
			// mainworld.player.didstunt = Math.random() > 0.3;
			mainworld.player.crouching = true;
		}
	}
	if (ev.key == "r") {
		mainworld.player.position.x = 0;
		mainworld.player.position.y = 0;
		mainworld.player.position.z = 0;
		mainworld.player.rotation.x = 0;
		mainworld.player.rotation.y = 0;
		mainworld.player.rotation.z = 0;
		mainworld.player.lane = 0;
		
	}
	if (ev.key == "b") {
		if (currently_using == CurrentlyUsingis.Colored) {
			shaderProgram = initShaderProgram(gl, vsSource, fsSource + greyscFs);
			currently_using = CurrentlyUsingis.Grayscale;
		} else {
			shaderProgram = initShaderProgram(gl, vsSource, fsSource + coloredFs);
			currently_using = CurrentlyUsingis.Colored;
		}
		refreshPrograminfo(gl, shaderProgram, programInfo);
	}
	if (ev.key == "f") {
		starttime = new Date();
		flicker = setInterval(
			() => mainworld.refreshShaders(), 100
		);
		window['flicker'] = flicker;
	}
	if (ev.key == "p") {
		mainworld.paused = !mainworld.paused;
	}
	// console.log(ev.key, ev.keyCode);
}


main();
