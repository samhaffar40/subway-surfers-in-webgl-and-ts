import { Jake } from "./jake";
import { Magnet } from "./magnet";
import { Coin } from "./coin";
import { Bird } from "./bird";
import { Jetpack } from "./jetpack";
import { initBuffers, drawObject3D, initShaderProgram, refreshPrograminfo, flashWallsShaders } from "./utils";
import { GL_BL_ASSETS } from "./init";
import { mat4 } from "gl-matrix";
import { Police } from "./police";
import { Dog } from "./dog";
import { Train } from "./train";
import { Tunnel } from "./tunnel";
import { Track } from "./tracks";
import { Pole } from "./pole";
import { Jumper } from "./jumper";
import { Ground } from "./ground";
import { Croucher } from "./croucher";
import { Jumponce } from "./jumponce";
import { Wall } from "./wall";

interface IDictionary<T> {
    [index:string]: T;
};

window['sclae'] = 1;

export class World{
    gl         : WebGLRenderingContext;
    flashprogram: Program;
    mainprogram: Program;
    program    : Program;
    player     : Jake;
    police     : Police; 
    cameraPos  : Vector3;
    buffers    : Buffer;
    object     : any;
    texture    : HTMLImageElement;
    vsSource   : string;
    fsSource   : string;
    tunnels?   : {
        doubletunnels?: any[],
        middletunnels?: Tunnel[],
        lefttunnels?  : any[],
        righttunnels? : any[],
        cointunnels?  : any[],
        bigtunnels?   : any[],
    };
    tracks        : Track[];
    obstacles     : {
        trains?     : Train[];
        poles?      : Pole[];
        crouchers?  : Croucher[];
        jumponces?  : Jumponce[];
    };
    jetpacks?   : Jetpack[];
    birds?      : Bird[];
    jumpers?    : Jumper[];
    magnets?    : Magnet[];
    grounds?    : Ground[];
    walls?      : Wall[];
    position    : Vector3;
    rotation    : Vector3;
    constructors: IDictionary<any>;
    paused      : boolean = false;
    lasttrainpos: Vector3;
    constructor(gl: WebGLRenderingContext, program : Program, cb: Function){
        // console.log('new jake baby');
        this.mainprogram = program;
        this.paused = false;
        // this.flashprogram = this.refreshShaders();
        this.program = program;
        this.gl = gl;

        var shders : string[] = flashWallsShaders();
        this.vsSource = shders[0];
        this.fsSource = shders[1];
        var tempProg : WebGLProgram = initShaderProgram(this.gl, this.vsSource, this.fsSource);
        this.flashprogram = {
            program: tempProg,
            attribLocations: {
                vertexPosition: gl.getAttribLocation(tempProg, 'aVertexPosition'),
                textureCoord: gl.getAttribLocation(tempProg, "aTextureCoord"),
                vertexNormal: gl.getAttribLocation(tempProg, 'aVertexNormal'),
                /* vertexColor: gl.getAttribLocation(tempProg, 'aVertexColor'), */
            },
            uniformLocations: {
                projectionMatrix: gl.getUniformLocation(tempProg, 'uProjectionMatrix'),
                modelViewMatrix: gl.getUniformLocation(tempProg, 'uModelViewMatrix'),
            },
        };
        this.player = new Jake({x:5,y:0,z:0}, this);
        this.police = new Police(this);
        this.cameraPos = {
            x:-2,
            y: 3,
            z: 0,
        };

        this.position = {
            x: 0,
            y: -6,
            z: 0,
        };
        this.rotation = {
            x: 0,
            y: 0,
            z: 0,
        };

        this.grounds = [];
        this.grounds.push(
            new Ground({x:0,y:-6,z:0}, this),
            new Ground({x:250,y:-6,z:0}, this),
            new Ground({x:500,y:-6,z:0}, this),
            new Ground({x:750,y:-6,z:0}, this),
            new Ground({x:1000,y:-6,z:0}, this),
            // new Ground({x:750,y:-6,z:0}, this),
        );

        /* All types of tunnels */
        this.tunnels = {
            doubletunnels : [],
            middletunnels: [new Tunnel({x:110,y:0,z:0},this,'tunnelmiddle')],
        };
        
        this.magnets = []; /* magnets */
        this.birds = [
            new Bird({x:10,y:10,z:10}, this),
            new Bird({x:10,y:10,z:-6}, this),
            new Bird({x:10,y:10,z:3}, this),
        ]; /* Birds */

        this.obstacles = {
            trains: [
                new Train({x:500,y:0,z:-20},this),
                new Train({x:1000,y:0,z:20},this),
                new Train({x:1450,y:0,z:20},this),
                new Train({x:2000,y:0,z:0},this),
                new Train({x:500,y:0,z:20},this),
            ],
        };

        this.tracks = [];
        for (var i=0; i< 20; i++){
            this.tracks.push(
                new Track({x:26*i,y:-3,z:-20},this),
                new Track({x:26*i,y:-3,z:  0},this),
                new Track({x:26*i,y:-3,z: 20},this),
            );
        }

        this.walls = [];
        for (var i=0; i< 20; i++){
            this.walls.push(
                new Wall({x:56*i,y:-3,z:-30},this.player),
                new Wall({x:56*i,y:-3,z:+30},this.player),
            );
        }

        this.jetpacks = [];
            this.jetpacks.push(
                new Jetpack({x: 300 ,y: 5,z:  0}, this.player),
                new Jetpack({x: 500 ,y: 5,z:  0}, this.player),
                new Jetpack({x: 2000,y: 5,z: -20}, this.player),
                new Jetpack({x: 3000,y: 5,z:-20}, this.player),
                new Jetpack({x: 3100,y: 5,z:  0}, this.player),
                new Jetpack({x: 3200,y: 5,z: 20}, this.player),
            );
        this.jetpacks.forEach(j=>j.candraw=true);

        this.jumpers = [];
        for (var i=0; i< 30; i++){
            var choice = Math.random();
            var offset: Vector3 = {
                x : 0,
                y : 0,
                z : 0,
            };
            offset.x = Math.random() * (3000 - 100) + 100;
            if (choice > 1/3){
                offset.z = -20;
            } else if (choice > 2/3){
                offset.z = 20;
            }
            var last = this.jumpers[this.jumpers.length-1];
            if (!last){
                last = new Jumper(this.player,{x:100,y:0,z:20});
                this.jumpers.push(last);
            }
            this.jumpers.push(
                new Jumper(
                    this.player,{
                    x: last.position.x + offset.x,
                    y: 0,
                    z: offset.z
                })
            );
        };

        this.obstacles.poles = [];
        for (var i=0; i< 10; i++){
            var choice = Math.random();
            var offset: Vector3 = {
                x : 0,
                y : 0,
                z : 0,
            };
            offset.x = Math.random() * (300 - 100) + 100;
            if (choice > 1/2){
                offset.z = -20;
            } else {
                offset.z = 20;
            }
            var lasta = this.obstacles.poles[this.obstacles.poles.length-1];
            if (!lasta){
                lasta = new Pole(this.player,{x:100,y:0,z:20});
                this.obstacles.poles.push(lasta);
            }
            this.obstacles.poles.push(
                new Pole(
                    this.player,{
                    x: lasta.position.x + offset.x,
                    y: 0,
                    z: offset.z
                })
            );
        };

        this.obstacles.jumponces = [];
        for (var i=0; i< 10; i++){
            var choice = Math.random();
            var offset: Vector3 = {
                x : 0,
                y : 0,
                z : 0,
            };
            offset.x = Math.random() * (10 - 0) + 0;
            if (choice < 1/3){
                offset.z = -20;
            } else if (choice < 2/3) {
                offset.z = 20;
            }
            var lastjumponce = this.obstacles.jumponces[this.obstacles.jumponces.length-1];
            if (!lastjumponce){
                console.log('WTF is wrong');
                lastjumponce = new Jumponce({x:130,y:0,z:20},this.player);
                this.obstacles.jumponces.push(lastjumponce);
            }
            this.obstacles.jumponces.push(
                new Jumponce({
                    x: lastjumponce.position.x + offset.x,
                    y: 0,
                    z: offset.z
                }, this.player)
            );
        };


        this.obstacles.crouchers = [];
        this.obstacles.crouchers.push(
            new Croucher({x:200,y:0,z:  0}, this.player),
            new Croucher({x:400,y:0,z: 20}, this.player),
            new Croucher({x:800,y:0,z:-20}, this.player),
        );
        
        this.constructors = {};
        this.constructors['jake'] = Jake;
        this.constructors['magnet'] = Magnet;
        this.constructors['coin'] = Coin;
        this.constructors['bird'] = Bird;
        this.constructors['dog'] = Dog;
        this.constructors['police'] = Police;
        this.constructors['pole'] = Pole;
        this.constructors['train'] = Train;
        this.constructors['track'] = Track;
        this.constructors['ground'] = Ground;

        // this.object  = GL_BL_ASSETS.objects['ground'];
        // this.texture = GL_BL_ASSETS.textures['ground'];
        // this.buffers = initBuffers(gl, this.object);

        cb(this);
    };

    refreshShaders() : void {

        var flash_on = Math.random() < 0.5;

        if (flash_on){
            this.program = this.flashprogram;
        } else {
            this.program = this.mainprogram;
        }
        
        this.gl.useProgram(this.program.program);
    };
/* 
    deleteBird(bird:Bird):void{
        let index : number = this.birds.indexOf(bird);
        if (index > -1) {
            this.birds.splice(index, 1);
        }
    }; */
    
    tickall():void{
        this.player.tick();
        this.police.tick();
        this.magnets.forEach((mag)=>{
            mag.tick();
        });

        var numbirds = this.birds.length;
        this.birds = this.birds.filter(bird=>!bird.dead); /* removing dead birds */
        if (this.birds.length < numbirds){
            this.birds.push( /* adding useless birds */
            new Bird({
                x: this.player.position.x + Math.random() * 30  ,
                y: this.player.position.y + 10,
                z: this.player.position.z + ((Math.random() > 0.5) ? -30 : 30) ,
            }, this));
        }

        var numtrains = this.obstacles.trains.length;
        this.obstacles.trains = this.obstacles.trains.filter(tr=>!tr.dead);
        if (numtrains>this.obstacles.trains.length){
            this.lasttrainpos = this.obstacles.trains[this.obstacles.trains.length - 1].position;
            var choice = Math.random();
            var offset: Vector3 = {
                x : 0,
                y : 0,
                z : 0,
            };
            offset.x = Math.random() * (600 - 300) + 300;
            if (choice > 1/3){
                offset.z = -20;
            } else if (choice > 2/3){
                offset.z = 20;
            } else {
                offset.z = 0;
            }
            this.obstacles.trains.push(
                new Train({
                    x: this.lasttrainpos.x + offset.x,
                    y: 0,
                    z: offset.z,
                }, this));
        }

        var numpoles = this.obstacles.poles.length;
        this.obstacles.poles = this.obstacles.poles.filter(pole=>{
            return (pole.position.x + 100 > this.player.position.x);
        });
        if (numpoles && numpoles > this.obstacles.poles.length){
            var choice_ = Math.random();
            var offset_: Vector3 = {
                x : 0,
                y : 0,
                z : 0,
            };
            offset_.x = Math.random() * (300 - 100) + 100;
            if (choice_ > 1/2){
                offset_.z = -20;
            } else {
                offset_.z = 20;
            }
            console.log(this.obstacles.poles.length-1, "lsat iindexxe")
            var lastpole = this.obstacles.poles[this.obstacles.poles.length-1];
            console.log(`Last pole in tickall ${lastpole} and num poles is ${numpoles}`);
            this.obstacles.poles.push(
                new Pole(
                    this.player,{
                    x: lastpole.position.x + offset_.x,
                    y: 0,
                    z: offset_.z
                })
            );
        }

        this.jetpacks.forEach(j=>j.tick());
        this.birds.forEach(bird=>bird.tick());
        this.obstacles.trains.forEach(train=>train.tick());

        this.tunnels.middletunnels.forEach(mid=>mid.tick());

        if(this.tracks[0].position.x + 100 < this.player.position.x){
            this.tracks.splice(0, 3);
            var nextposx = this.tracks[this.tracks.length - 1].position.x + 26;
            this.tracks.push(
                new Track({x:nextposx,y:-3,z:-20}, this),
                new Track({x:nextposx,y:-3,z:  0}, this),
                new Track({x:nextposx,y:-3,z: 20}, this)
            );
        }

        if(this.grounds[0].position.x + 250 < this.player.position.x){
            this.grounds.splice(0, 1);
            var nextposx = this.grounds[this.grounds.length - 1].position.x + 250;
            this.grounds.push(
                new Ground({x:nextposx,y:-6,z:0}, this),
                new Ground({x:nextposx,y:-6,z:0}, this),
                new Ground({x:nextposx,y:-6,z:0}, this)
            );
        }

        if(this.walls[0].position.x + 100 < this.player.position.x){
            this.walls.splice(0, 2);
            var nextposx = this.walls[this.walls.length - 1].position.x + 56;
            this.walls.push(
                new Wall({x:nextposx,y:-3,z:-30}, this.player),
                new Wall({x:nextposx,y:-3,z:+30}, this.player)
            );
        }

        this.tracks.forEach(track=>track.tick());
        this.obstacles.jumponces.forEach(j=>j.tick());
        this.jumpers.forEach(j=>j.tick());
        this.jumpers = this.jumpers.filter(j=>!j.dead);
        this.jetpacks = this.jetpacks.filter(j=>!j.dead);
    };

    
    nearestTrains() : Train[] {
        this.obstacles.trains.sort(
            (train1, train2)=>{
            if(train1.position.x > train2.position.x){
                return 1;
            }else {
                return -1;
            }
        });

        return [
            this.obstacles.trains[0],
            this.obstacles.trains[1],
        ];
    }

    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        // draw this.object;
        // drawObject3D(this.gl, this.program, projectionMatrix, this);
        this.grounds.forEach((grnd)=>{
            grnd.draw(projectionMatrix, programinfo, deltatime);
        });
        this.player.draw(projectionMatrix, (this.player.program || programinfo), deltatime);
        
        this.police.draw(projectionMatrix, (this.mainprogram || programinfo), deltatime);
        
        this.tunnels.middletunnels.forEach((mid)=>{
            mid.draw(projectionMatrix, this.mainprogram || programinfo, deltatime);
        });
        this.obstacles.trains.forEach((train)=>{
            train.draw(projectionMatrix, (this.mainprogram || programinfo), deltatime);
        });
        this.birds.forEach((bird)=>{
            bird.draw(projectionMatrix, (this.mainprogram || programinfo), deltatime);
        });
        this.tracks.forEach((track)=>{
            // console.log("Track baby", track);
            track.draw(projectionMatrix, this.mainprogram, deltatime);
        });
        this.jumpers.forEach((hump)=>{
            hump.activate();
            hump.draw(projectionMatrix, this.mainprogram, deltatime);
        });
        this.obstacles.poles.forEach((poles)=>{
            poles.draw(projectionMatrix, this.mainprogram, deltatime);
        });
        
        this.obstacles.crouchers.forEach((croucher)=>{
            croucher.draw(projectionMatrix, this.mainprogram, deltatime);
        });

        this.obstacles.jumponces.forEach((jumponce)=>{
            jumponce.draw(projectionMatrix, this.mainprogram, deltatime);
        });

        this.walls.forEach(wall=>{
            wall.draw(projectionMatrix, this.mainprogram, deltatime);
        });
        this.jetpacks.forEach(jetpack=>{
            jetpack.draw(projectionMatrix, this.mainprogram, deltatime);
        });
        
    };
};
