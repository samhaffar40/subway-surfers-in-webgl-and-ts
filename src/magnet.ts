import {Jake} from "./jake";
import { GL_BL_ASSETS } from "./init";
import { drawObject3D, initBuffers } from "./utils";
import { mat4 } from "gl-matrix";
import { World } from "./world";

export class Magnet{
    world     : World;
    player    : Jake;
    active    : boolean = false;
    candraw   : boolean = false;
    object    : any;
    position : Vector3;
    rotation  : Vector3;
    texture   : HTMLImageElement;
    gl        : WebGLRenderingContext;
    buffers   : Buffer;
    constructor(player: Jake, position: Vector3){
        this.player = player;
        this.gl = player.gl;
        this.world = player.world;
        this.active = false;
        this.position = position;
        this.object = GL_BL_ASSETS.objects['magnet'];
        this.texture = GL_BL_ASSETS.textures['magnet'];
        this.buffers = initBuffers(this.gl, this.object);
    }

    activate():void{
        if (!this.active){
            this.active = true;
            this.candraw = true;
            console.log('Activating magnet');
        } else {
            console.log('Already active');
        }
    }

    deactivate():void{
        this.candraw = false;
        this.active = false;
    }

    tick():void{
        /* rotate about y ? */
        this.rotation.y += 0.1;
    };
    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        // draw this.object;
        if (this.active)
        drawObject3D(this.gl, programinfo, projectionMatrix, this);
    }
}
