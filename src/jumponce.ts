import {Jake} from "./jake";
import { GL_BL_ASSETS } from "./init";
import { drawObject3D, initBuffers } from "./utils";
import { mat4 } from "gl-matrix";
import { World } from "./world";

export class Jumponce{
    world     : World;
    player    : Jake;
    dead      : boolean = false;
    object    : any;
    position : Vector3;
    rotation  : Vector3;
    texture   : HTMLImageElement;
    gl        : WebGLRenderingContext;
    buffers   : Buffer;
    bbox      : Bbox;
    constructor(position: Vector3, player: Jake){
        this.player = player;
        this.gl = player.gl;
        this.world = player.world;
        this.position = position;
        this.rotation = {
            x:0, y:0, z:0
        };

        this.bbox = {
            height : 1.63,
            length : 7.33,
            width  : 5.7,
        };

        this.object = GL_BL_ASSETS.objects['jumponce'];
        this.texture = GL_BL_ASSETS.textures['jumponce'];
        this.buffers = initBuffers(this.gl, this.object);
    }
    tick():void{
        /* rotate about y ? */
        if (this.position.x + 100 < this.world.player.position.x){
            this.dead = true;
        }
    };
    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        // draw this.object;
        // console.log('Drawing myself');
        drawObject3D(this.gl, programinfo, projectionMatrix, this);
    }
}
