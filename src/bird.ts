import { GL_BL_ASSETS } from "./init";
import { loadImg, loadObj, initBuffers, drawObject3D } from "./utils";

import { World } from "./world";
import { mat4 } from "gl-matrix";

export class Bird{
    position   : Vector3;
    rotation   : Vector3;
    initpos    : Vector3;
    candraw    : boolean;
    object     : any;
    texture    : HTMLImageElement;
    buffers    : Buffer;
    textureSlot: number;
    dead       : boolean = false;
    speed      : Vector3;
    world      : World;
    gl         : WebGLRenderingContext;
    constructor(position: Vector3, world : World){
        this.position = position;
        this.world = world;
        this.gl = world.gl;
        this.speed = {
            x: 0.4,
            y:Math.random()*2, /* 1-11 float */
            z: 0.2
        };
        this.rotation = {
            x: 0,
            y: 0,
            z: 0
        };
        this.initpos = {
            x : position.x,
            y : position.y,
            z : position.z,
        };

        this.object = GL_BL_ASSETS.objects['bird'];
        this.texture = GL_BL_ASSETS.textures['bird'];
        this.buffers = initBuffers(this.gl, this.object);

    }

    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        drawObject3D(this.gl, programinfo, projectionMatrix, this);
    };

    tick(position?: Vector3):void{
        //move randomly
        if (this.isdead()){
            // this.world.deleteBird(this);
            this.dead = true;
            return;
        }
        this.position.x += this.speed.x;
        this.position.y += this.speed.y;
        this.position.z += this.speed.z;
    }

    isdead() : boolean {
        /* check collsions and whether it left the viewport*/
        if (
            this.position.x - this.world.player.position.x <100 &&
            this.position.x - this.world.player.position.x >-100 &&
            this.position.y - this.world.player.position.y <100 &&
            this.position.y - this.world.player.position.y >-100 &&
            this.position.z - this.world.player.position.z >-100 &&
            this.position.z - this.world.player.position.z <100
           ){
            return false;
        }
        return true;
    };
}