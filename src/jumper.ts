import {Jake} from "./jake";
import { GL_BL_ASSETS } from "./init";
import { mat4 } from "gl-matrix";
import { drawObject3D, initBuffers } from "./utils";

export class Jumper{
    player : Jake;
    active : boolean;
    candraw: boolean;
    dead   : boolean = false;
    object : any;
    gl     : WebGLRenderingContext;
    position: Vector3;
    rotation: Vector3;
    buffers : Buffer;
    texture: HTMLImageElement;
    bbox    : Bbox;
    lane    : number;
    constructor(player: Jake, position: Vector3){
        this.player = player;
        this.active = false;
        this.candraw = false;
        this.gl = player.gl;
        this.position = position;
        this.rotation = {
            x: 0,
            y: 180,
            z: 0
        };
        this.lane = this.position.z / 20;

        this.bbox = {
            width  : 2.4,
            height : 3,
            length : 5,
        };

        this.object = GL_BL_ASSETS.objects['jumper'];
        this.texture = GL_BL_ASSETS.textures['jumper'];
        this.buffers = initBuffers(this.gl, this.object);
    }

    activate():void{
        if (!this.active){
            console.log("Activating shoes");
            this.candraw = true;
            this.active = true;
            if (this.player.shoes.active){ /* deactivate if shoes is players' */
                setTimeout(() => {
                    this.deactivate();
                }, 20000); /* Deactivate shoes after 20 sec */
            }
        }
        // else console.log("Already active");
        return;
    }

    deactivate():void{
        this.candraw = false;
        this.active = false;
    }

    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        if (this.candraw && this.active){
            /* draw */
            drawObject3D(this.gl, programinfo, projectionMatrix, this);
        }
    }

    tick(){
        (this.position.x + 100 < this.player.position.x) && (this.dead = true);
    }
}
