import { GL_BL_ASSETS } from "./init";
import { initBuffers, drawObject3D } from "./utils";

import { World } from "./world";
import { mat4 } from "gl-matrix";
import { Police } from "./police";

export class Dog{
    position: Vector3;
    rotation: Vector3;
    speed: Vector3;
    texture: HTMLImageElement;
    object: any;
    world: World;
    gl: WebGLRenderingContext;
    buffers: Buffer;
    lane: number;
    program: Program;
    distance: number;
    owner   : Police;
    clebrating: number = 0.3;
    constructor(police: Police, program?: Program) {
        if (program) {
            this.program = program;
        }
        this.world = police.world;
        this.gl = police.gl;

        this.owner = police;
        this.position = { x: police.position.x, y: police.position.y, z: police.position.z - 5 };

        this.lane = police.lane;
        this.speed = {
            x: 0.5,
            y: 0.3,
            z: 0
        };
        this.rotation = {
            x: 0,
            y: -90,
            z: 0
        };

        this.object = GL_BL_ASSETS.objects['dog'];
        this.texture = GL_BL_ASSETS.textures['dog'];
        this.buffers = initBuffers(this.gl, this.object);
    }

    tick(data?: any): void | boolean {
        this.lane = this.world.player.lane;
        if (this.world.player.dead){
            if (this.rotation.x > 10 || this.rotation.x < -10){
                this.clebrating = -this.clebrating;
            }
            this.rotation.x += this.clebrating;
        }
        this.position.x = this.owner.position.x;
        this.position.y = this.owner.position.y;
        if (this.position.y > 20){
            this.position.y = 0;
        }
        this.position.z = this.owner.position.z - (5);
    }
    draw(projectionMatrix: mat4, programinfo: Program, deltatime: number): void {
        // draw this.object;
        drawObject3D(this.gl, programinfo, projectionMatrix, this);
    };
};
