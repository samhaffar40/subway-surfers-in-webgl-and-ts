# Subawy Surf in Typescript using WebGL

## Controls

* A/D keys to change lanes.
* Space/Up keys to jump.
* S click key to activate a scateboard.
* B to toggle grayscale

## TODO [Implement]

* Magnets
* Jetpacks

## Done

* Jake
* World
* Coins
* Walls
* Textures

* Police, Dog

## Strategy / Plan to bring this game out

1. World
    * Player
    * Layout
        * Birds
        * Powerups
            * Mystery boxes
            * Jumpers
            * Magnets
            * Jetpacks
        * Tunnels
            * Middle Tunnel
            * Coin Tunnel
            * Big tunnel
            * Side tunnel.L
            * Side tunnel.R
            * Double Tunnel
        * Trains
            * Slant
            * Box
            * Front
        * Poles
        * Obstacles
            * Jumponce
            * Croucher
            * BigCroucher
            * Rock
            * Dustbin
    * Score
        * Points
        * Coins collected

2. Player
    * Dead/Alive (Head on collisions)
    * Confused (Indirect collisions)
    * ScateBoard
    * Equipped_Magnet
    * Equipped_Jetpack
    * Equipped_Jumpers
    * Vector3
    * Position
    * buffers
    * Texture

3. Police
    * Dog
