import { GL_BL_ASSETS } from "./init";
import { drawObject3D, initBuffers } from "./utils";
import { mat4 } from "gl-matrix";
import { World } from "./world";
import { Coin } from "./coin";

export class Track{
    world     : World;
    dead      : boolean = false;
    object    : any;
    position : Vector3;
    rotation  : Vector3;
    texture   : HTMLImageElement;
    gl        : WebGLRenderingContext;
    buffers   : Buffer;
    textureSlot: number;
    coins      : Coin[];
    speed      : Vector3;
    constructor(position: Vector3, world: World){
        this.gl = world.gl;
        this.world = world;
        this.position = position;
        this.rotation = {
            x:0,
            y:0,
            z:0,
        };
        this.speed = {
            x:0,
            y:0,
            z:0,
        };
        this.coins = [];
        if (Math.random() > 0.91){
            for (var i:number =0 ; i< 8 ;i ++){
                this.coins.push(
                new Coin(
                    {
                        x: this.position.x + (i - 4) * 6,
                        y: this.position.y + 5,
                        z: this.position.z,
                    },
                    world.player, this)
                );
            }
        }    

        this.object = GL_BL_ASSETS.objects['track'];
        this.texture = GL_BL_ASSETS.textures['track'];
        this.buffers = initBuffers(this.gl, this.object);
    }

    deleteCoin(coin: Coin):void{ 
        let index : number = this.coins.indexOf(coin);
        if (index > -1) {
            this.coins.splice(index, 1);
        }
    }

    tick():void{
        this.coins.forEach(coin=>coin.tick());
    }

    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        // draw this.object;
        drawObject3D(this.gl, programinfo, projectionMatrix, this);
    
        this.coins.forEach(coin=>{
            coin.draw(projectionMatrix, programinfo, deltatime);
        });
    };
}
