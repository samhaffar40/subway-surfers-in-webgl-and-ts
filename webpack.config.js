var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyPlugin = require('copy-webpack-plugin');
const path = require('path');
const variable = require('./webpack.vars');
// const shell = require('shelljs');

// shell.cd('src/');
// shell.exec('bash scripts/refreshAssets.sh');
// shell.cd('..');

module.exports = (env, argv) => {
	var configSrcMap = false;
	var devmode = false;
	var OutBndlFile = "bundle.prod.js";
	devmode = true;
	configSrcMap = 'inline-source-map';
	
	if (argv.mode == "development" || env == "development" || process.env.NODE_ENV === 'development'){
		OutBndlFile = "bundle.dev.js";
		process.env.NODE_ENV = "development";
	}

	return {
		entry: './src/main.ts',
		devtool: configSrcMap,
		mode: 'development',
		module: {
			rules: [
				{
					test: /\.tsx?$/,
					use: 'ts-loader',
					exclude: /node_modules/
				},
				{
					test: /\.(png|jpg|jpeg|gif|ico)$/,
					use: [
						{
							loader: 'file-loader',
							options: {
								// name :  (devmode) ? 'images/[name].[ext]': 'images/[hash].[ext]'
								name(_file) {
									console.log("A Png.."+_file);
									return  (devmode) ? 'images/[name].[ext]': 'images/[hash].[ext]';
								},
							},
						},
					],
				},
				{
					test: /\.(obj)$/,
					use: [
						{
							loader: 'file-loader',
							options: {
								// name :  (devmode) ? 'obj/[name].[ext]': 'obj/[hash].[ext]'
								name(_file) {
									console.log(`A Obj file ${_file}`);
									return  (devmode) ? 'obj/[name].[ext]': 'obj/[hash].[ext]';
								},
							},
						},
					],
				},
			]
		},
		resolve: {
			extensions: [ '.tsx', '.ts', '.js' ]
		},
		output: {
			filename: OutBndlFile,
			path: path.resolve(__dirname, 'dist')
		},
		plugins: [
			new CopyPlugin([{
				from: 'src/icons/',
				to: '[name].[ext]',
				toType: 'template',
			},]),
			new HtmlWebpackPlugin({
				template : 'index.html',
				title    : variable.title,
				desc     : variable.desc,
				// scriptsrc: `../${OutBndlFile}`
			}),
		],
		devServer : {
			compress : true,
		},
/* 		externals : {
			'gl-matrix' : {
				root : 'window'
			}
		} */
	}
};
